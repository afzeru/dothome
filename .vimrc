" TODO:
" - install Pathogen
" - install vim-airline

" General stuff
set autoread
set magic

" User interface
syntax on
filetype on
set nu
set ruler
set expandtab
set shiftwidth=2
set tabstop=2
set smarttab
set ai "Auto indent
set si "Smart indent
colorscheme ron		" aafranco likes this scheme
"colorscheme murphy      " good Irish colors, to keep us on our toes

" Filetypes
au BufNewFile,BufRead *.tt set filetype=mason

" Fixes
set nobackup
set nowb
set noswapfile
set backspace=eol,start,indent
imap  <Left><Del>
set pastetoggle=<F2>
set nopaste

" Blatantly stolen from plxs
set nocompatible        " Use Vim defaults (much better!)
set bs=2                " allow backspacing over everything in insert mode
set is                  " incremental search
set showmode ic         " show mode, ignore case when searching
set hlsearch            " highlighted search

map F !} fmt -78 -c     " map F to command to break a line into
                        " multiple lines no longer that 78 characters

" More twaks - bar and buffer stuff
set laststatus=2
set bufhidden=unload
set history=50

